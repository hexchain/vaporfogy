#!/usr/bin/env python
# vim: set fileencoding=utf-8:

"""A lightweight online judge system backend."""

__author__ = 'Hexchain Tong <richard0053@gmail.com>'
__version__ = '0.1-alpha'

import os
import re
import subprocess
import sys

_re_validinclude = re.compile(r'^#include\s*[<"](?:stdio\.h|stdlib\.h|math\.h|ctype\.h|string\.h|errno\.h|limits\.h|float\.h|iostream|string|iterator|bitset)[>"]\s*$', re.M)

def _error(err, noargv=False):
    if noargv:
        print >> sys.stderr, '%s' % str(err)
    else:
        print >> sys.stderr, '%s: %s' % (sys.argv[0], str(err))
    exit(1)

def _verify(fsrc):
    src = fsrc.read()
    src = src.replace('\r', '\n').replace('\\\n', '')
    src = _re_validinclude.sub('', src)
    return not re.search(r'^#include', src, re.M)

def _compile(filename):
    suffix = re.search(r'\.(c|cpp|pas)$', filename).group(1)
    if suffix == 'c':
        cmd = 'gcc %s -Wall -static -o %s.out -lm' %(filename, filename)
    elif suffix == 'cpp':
        cmd = 'g++ %s -Wall -static -o %s.out -lm' %(filename, filename)
    elif suffix == 'pas':
        cmd = 'fpc %s -Xt -o%s.out' %(filename, filename)
    cmd = 'timeout 30 ' + cmd
    proc = subprocess.Popen(cmd.split(' '), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    proc.wait()
    return proc.returncode, proc.stdout

def _run(filename, timelimit, memlimit, stdin):
    proc = subprocess.Popen(('./fork %d %d %s' % (timelimit, memlimit, filename)).split(' '), stdin=stdin, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc.wait()
    return proc.returncode, proc.stdout, proc.stderr

def _compare(fout, fans):
    out = [line.strip() for line in fout if line.strip()]
    ans = [line.strip() for line in fans if line.strip()]
    diff = len(out) - len(ans)
    return ans == out, diff

def main():
    try:
        filename = sys.argv[1]
        inmask = sys.argv[2]
        ansmask = sys.argv[3]
        case = int(sys.argv[4])
        timelimit = int(sys.argv[5])
        memlimit = int(sys.argv[6])

        fsrc = open(filename)
        fin = {}
        fans = {}
        for i in range(1, case + 1):
            fin[i] = open(inmask % i)
            fans[i] = open(ansmask % i)
    except (IndexError, ValueError, TypeError):
        _error('usage: %s filename inmask ansmask case timelimit memlimit' % sys.argv[0], True)
    except IOError as ex:
        _error('%s: %s' % (ex.filename, ex.strerror))

    if not _verify(fsrc):
        print 'CE: Rejected, illegal headers'
        exit(0)

    ret = _compile(filename)
    if ret[0]:
        print 'CE'
        print ret[1].read().rstrip()
        exit(0)

    stat = {}
    ac = 0
    wa = 0
    tle = 0
    rte = 0
    oops = 0
    for i in range(1, case + 1):
        ret = _run('%s.out' % filename, timelimit, memlimit, fin[i])
        if ret[0] != 0:
            if ret[0] == 123: # problem forking XXX
                oops += 1
                stat[i] = 'Oops: %s' % ret[2].read().strip()
            elif ret[0] == 124: # timeout XXX
                tle += 1
                stat[i] = 'TLE'
            elif ret[0] < 0: # signal
                rte += 1
                sig = -ret[0]
                stat[i] = 'RTE: Killed by signal %d' % sig
            else:
                rte += 1
                stat[i] = 'RTE: Program returned %d' % ret[0]
        else:
            diff = _compare(ret[1], fans[i])
            if diff[0]:
                ac += 1
                stat[i] = False
            else:
                wa += 1
                stat[i] = 'WA: '
                if diff[1] > 0:
                    stat[i] += 'Output is longer than answer'
                elif diff[1] < 0:
                    stat[i] += 'Output is shorter than answer'
                else:
                    stat[i] += 'Output is different from answer.'

    if ac == 10:
        print 'AC'
    else:
        print "%d AC, %d WA, %d TLE, %d RTE" % (ac, wa, tle, rte)
        for i in range(1, case + 1):
            if stat[i]:
                print 'Point %d: %s' % (i, stat[i])
    try:
        os.remove("%s.out" % filename)
    except OSError:
        pass

if __name__ == '__main__':
    main()
