#!/usr/bin/env python
# vim: fileencoding=utf-8:

import MySQLdb
import os
import subprocess
import sys
import time

from config import dbargs, wd, interval
from daemon import Daemon

class Queue(Daemon):
    def run(self):
        db = MySQLdb.connect(**dbargs)
        c = db.cursor()
        c.execute('''
            UPDATE `vf_queue`
            SET `status` = '4' /* EINTR */, `backmsg` = ''
            WHERE `status` = '2';
        ''')
        while True:
            c.execute('''
                SELECT
                    `rcid`,
                    `filename`,
                    `inmask`,
                    `ansmask`,
                    `case`,
                    `timelimit`,
                    `memlimit`
                FROM `vf_queue`
                JOIN `vf_problems`
                WHERE `vf_queue`.`prid` = `vf_problems`.`prid`
                AND `status` = '1'
                ORDER BY `rcid` ASC
                LIMIT 1;
            ''')
            try:
                row = c.fetchall()[0]
            except IndexError:
                pass
            else:
                # usage: ./vaporfogy.py filename inmask ansmask case timelimit memlimit
                rcid = row[0]
                filename = row[1]
                inmask = row[2]
                ansmask = row[3]
                case = row[4]
                timelimit = row[5]
                memlimit = row[6]
                c.execute('''
                    UPDATE `vf_queue` SET `status` = '2'
                    WHERE `rcid` = '%s';
                ''' % rcid)
                # XXX path to vaporfogy.py?
                cmd = './vaporfogy.py %s %s %s %s %s %s' % (filename, inmask, ansmask, case, timelimit, memlimit)
                proc = subprocess.Popen(cmd.split(' '), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                proc.wait()
                stdout = db.escape_string('\n'.join([line.rstrip() for line in proc.stdout]))
                c.execute('''
                    UPDATE `vf_queue`
                    SET `status` = '0', `backmsg` = '%s'
                    WHERE `rcid` = '%s';
                ''' % (stdout, rcid))
                try:
                    os.remove(filename)
                except OSError:
                    pass

            time.sleep(interval)

if __name__ == '__main__':
    daemon = Queue(wd + 'pid/queue.pid', chdir=wd, stderr=wd + 'logs/error.log')
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            print >> sys.stderr, 'usage: %s start|stop|restart' % sys.argv[0]
            sys.exit(2)
        sys.exit(0)
    else:
        print >> sys.stderr, 'usage: %s start|stop|restart' % sys.argv[0]
        sys.exit(2)
