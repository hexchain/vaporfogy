#!/usr/bin/env python
# vim: set fileencoding=utf-8:

dbargs = {
    'host': 'HOST',
    'user': 'USER',
    'passwd': 'PASSWD',
    'db': 'DB',
    # Never change the following lines!
    'charset': 'utf8',
    'use_unicode': False
}

wd = '/path/to/wd/'
interval = 10
