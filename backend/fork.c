#include <assert.h>
#include <dirent.h>
#include <errno.h>
#include <libgen.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#ifdef EXIT_FAILURE
    #undef EXIT_FAILURE
#endif
#define EXIT_FAILURE 123
#define EXIT_TIMEDOUT 124

pid_t cpid = -2;
char *self;
int timed_out = 0;

void err(const char *s) {
    char prefix[1024];
    sprintf(prefix, "%s: %s", self, s);
    perror(prefix);
}

void killchild() {
    if (0 != kill(cpid, SIGKILL)) {
        if (!(ESRCH == errno)) {
            err("kill");
            exit(EXIT_FAILURE);
        }
    }
}

void sig_alrm(int signum) {
    killchild();
    timed_out = 1;
}

int main(int argc, char *argv[]) {
    self = argv[0];
    if (0 != geteuid()) {
        fprintf(stderr, "%s: need superuser privileges.\n", self);
        exit(EXIT_FAILURE);
    }
    if (4 != argc) {
        fprintf(stderr, "usage: %s timelimit memlimit a.out\n", self);
        exit(EXIT_FAILURE);
    }
    unsigned int timelimit;
    unsigned long memlimit;
    if ((0 == sscanf(argv[1], "%u", &timelimit) || 0 == timelimit) || (0 == sscanf(argv[2], "%lu", &memlimit) || 0 == memlimit)) {
        fprintf(stderr, "usage: %s timelimit memlimit a.out\n", self);
        exit(EXIT_FAILURE);
    }
    char *basec, *base;
    basec = strdup(argv[3]);
    base = basename(basec);
    char newpath[256] = "root/";
    DIR *dp;
    struct dirent *ep;
    if (-1 == chdir(newpath)) {
        err("chdir");
        exit(EXIT_FAILURE);
    }
    if (NULL == (dp = opendir("./"))) {
        err("opendir");
        exit(EXIT_FAILURE);
    }
    while ((ep = readdir(dp))) {
        if (strcmp(".", ep->d_name) && strcmp("..", ep->d_name)) {
            if (-1 == unlink(ep->d_name)) {
                err("unlink");
                exit(EXIT_FAILURE);
            }
        }
    }
    closedir(dp);
    if (-1 == chdir("../")) {
        err("chdir");
        exit(EXIT_FAILURE);
    }
    strcat(newpath, base);
    if (-1 == link(argv[3], newpath)) {
        err("link");
        exit(EXIT_FAILURE);
    }
    if (-1 == (cpid = fork())) {
        err("fork");
        exit(EXIT_FAILURE);
    } else if (0 == cpid) {
        struct rlimit rlim;
        rlim.rlim_cur = 0;
        rlim.rlim_max = 0;
        if (-1 == setrlimit(RLIMIT_FSIZE, &rlim)) {
            err("setrlimit");
            exit(EXIT_FAILURE);
        }
        rlim.rlim_cur = 1;
        rlim.rlim_max = 1;
        if (-1 == setrlimit(RLIMIT_NPROC, &rlim)) {
            err("setrlimit");
            exit(EXIT_FAILURE);
        }
        rlim.rlim_cur = memlimit;
        rlim.rlim_max = memlimit;
        if (-1 == setrlimit(RLIMIT_AS, &rlim)) {
            err("setrlimit");
            exit(EXIT_FAILURE);
        }
        if (-1 == nice(10)) {
            err("nice");
            exit(EXIT_FAILURE);
        }
        if (-1 == chroot("root")) {
            err("chroot");
            exit(EXIT_FAILURE);
        }
        if (-1 == chdir("/")) {
            err("chdir");
            exit(EXIT_FAILURE);
        }
        if (-1 == setuid(65534)) {
            err("setuid");
            exit(EXIT_FAILURE);
        }
        char execpath[256] = "/";
        strcat(execpath, base);
        char *newargv[] = { base, NULL };
        char *newenvp[] = { NULL };
        execve(execpath, newargv, newenvp);
        err("execve");
        exit(EXIT_FAILURE);
    } else {
        atexit(killchild);
        if (SIG_ERR == signal(SIGALRM, sig_alrm)) {
            err("signal");
            exit(EXIT_FAILURE);
        }
        alarm(timelimit);
        int status = 0;
        if (-1 == waitpid(cpid, &status, 0)) {
            err("waitpid");
            exit(EXIT_FAILURE);
        }
        if (SIG_ERR == signal(SIGALRM, SIG_DFL)) {
            err("signal");
            exit(EXIT_FAILURE);
        }
        if (-1 == unlink(newpath)) {
            err("unlink");
            exit(EXIT_FAILURE);
        }
        if (timed_out) {
            exit(EXIT_TIMEDOUT);
        }
        if (WIFEXITED(status)) {
            exit(WEXITSTATUS(status));
        } else if (WIFSIGNALED(status)) {
            raise(WTERMSIG(status));
        } else {
            exit(EXIT_SUCCESS);
        }
    }
    assert(0);
}
