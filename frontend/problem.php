<?php
require_once "config.php";
mysql_connect(DB_HOST, DB_USER, DB_PASS) or die(mysql_error());
mysql_select_db(DB_NAME) or die(mysql_error());
mysql_query('SET NAMES utf8;') or die(mysql_error());
session_start();

if(isset($_GET['prid'])) { $prid = (int)$_GET['prid']; } else { $prid = 0; };
if(isset($_GET['page'])) { $page = (int)$_GET['page']; } else { $page = 0; };

if(!$prid) {
    $sql = 'SELECT COUNT(*) FROM `vf_problems`;';
    $total = mysql_fetch_assoc(mysql_query($sql));
    $total = $total['COUNT(*)'];
    $pages = (int)max(1, ceil($total / 20));
    if($page < 1) { $page = 1; };
    if($page > $pages) { $page = $pages; };
    $sql = 'SELECT * FROM `vf_problems` ORDER BY `prid` ASC LIMIT ' . ($page - 1) * 20 . ', 20;';
    $result = mysql_query($sql);
    ?><!doctype html><meta charset=utf-8><title>题目列表</title><link rel=stylesheet type=text/css href=style.css /><h1>题目列表</h1><?php
    if(!isset($_SESSION['uid']) || $_SESSION['uid'] == "") {
?><a href=signin.php?redir=problem.php>登录</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href=signup.php?redir=problem.php>注册</a><br/><?php
    } else {
?>用户ID: <?=$_SESSION['uid']; ?>&nbsp;&nbsp;&nbsp;&nbsp;<a href=signout.php?redir=problem.php>注销</a><br/><?php
    }
?><table class=table><thead><tr><th>ID<th>名称<th>标题<tbody><?php
    while($row = mysql_fetch_assoc($result)) {
?><tr><td><?=$row['prid'];?><td><a href='?prid=<?=$row['prid'];?>'><?=$row['name'];?></a><td><?=$row['title'];?><?php
    }
?></table><p><?php
    if($page == 1) {
?>&lt;Prev <?php
    } else {
?><a href='?page=<?=$page - 1;?>'>&lt;Prev</a> <?php
    }
    for($i = 1; $i <= $pages; $i++) {
        if($i == $page) {
?><?=$i;?> <?php
        } else {
?><a href='?page=<?=$i;?>'><?=$i;?></a> <?php
        }
    }
    if($page == $pages) {
?>Next&gt;<?php
    } else {
?><a href='?page=<?=$page + 1;?>'>Next&gt;</a><?php
    }
?><hr /><p><small>Copyright &copy; Hexchain Tong, 2011.</small><?php
} else {
    $sql = 'SELECT * FROM `vf_problems` WHERE `prid` = ' . $prid . ';';
    $result = mysql_query($sql);
    $row = mysql_fetch_assoc($result);
    if($row) {
?><!doctype html><meta charset=utf-8><title>题目描述 - <?=$row['title'];?></title><link rel=stylesheet type=text/css href=style.css /><h1>题目描述 - <?=$row['title'];?></h1><h2><code><?=$row['name'];?></code> - <a href='submit.php?prid=<?=$row['prid'];?>'><small>提交程序</small></a></h2><p class=sub>题目描述：<blockquote><?=nl2br(htmlspecialchars($row['descr']), true);?></blockquote><p class=sub>样例输入：<blockquote><?=htmlspecialchars($row['samplein']);?></blockquote><p class=sub>样例输出：<blockquote><?=htmlspecialchars($row['sampleout']);?></blockquote><p class=sub>样例解释：<blockquote><?=nl2br(htmlspecialchars($row['sampledesc']), true);?></blockquote><p class=sub>数据范围：<blockquote><?=nl2br(htmlspecialchars($row['caserange']), true);?></blockquote><p class=sub>运行限制：<blockquote>最多执行 <?=$row['timelimit'];?> 秒，使用 <?=$row['memlimit'];?> 字节内存。</blockquote><p class=sub>题目来源：<blockquote><?=nl2br(htmlspecialchars($row['source']), true);?></blockquote><hr /><p><small>Copyright &copy; Hexchain Tong, 2011.</small><?php
    } else {
        header('HTTP/1.0 404 Not Found');
?><!doctype html><meta charset=utf-8><title>题目描述</title><h1>题目 ID 不存在</h1><hr /><p><small>Copyright &copy; Hexchain Tong, 2011.</small><?php
    }
}
?>
