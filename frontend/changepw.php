<?php
require_once "config.php";
require_once "recaptchalib.php";
mysql_connect(DB_HOST, DB_USER, DB_PASS) or die(mysql_error());
mysql_select_db(DB_NAME) or die(mysql_error());
mysql_query('SET NAMES utf8;') or die(mysql_error());
session_start();

if(!isset($_SESSION['uid'])) {
    header('Location: changepw.php');
    exit();
}

switch($_SERVER['REQUEST_METHOD']) {
case 'POST':
    if(isset($_POST['oldpw'])) { $oldpw = trim($_POST['oldpw']); } else { $oldpw = ''; };
    if(isset($_POST['newpw'])) { $newpw = trim($_POST['newpw']); } else { $newpw = ''; };
    if(isset($_POST['newpwconf'])) { $newpwconf = trim($_POST['newpwconf']); } else { $newpwconf = ''; };
    if(isset($_POST['recaptcha_challenge_field'])) { $recaptcha_challenge_field = $_POST['recaptcha_challenge_field']; } else {$recaptcha_challenge_field = ''; };
    if(isset($_POST['recaptcha_response_field'])) { $recaptcha_response_field = $_POST['recaptcha_response_field']; } else {$recaptcha_response_field = ''; };

    if((!$oldpw) || (!$newpw) || (!$newpwconf) || (!$recaptcha_challenge_field) || (!$recaptcha_response_field)) {
        $error = '缺少参数。';
        break;
    }
    if(strlen($newpw) >= 64) {
        $error = '密码过长。';
        break;
    }
    if($newpw != $newpwconf) {
        $error = '两次输入密码不一致。';
        break;
    }

    $resp = recaptcha_check_answer(RECAPTCHA_PRIVKEY, $_SERVER["REMOTE_ADDR"], $recaptcha_challenge_field, $recaptcha_response_field);
    if (!$resp->is_valid) {
        $error = '验证码错误。';
        break;
    }

    $sql = "SELECT `password` FROM `vf_users` WHERE `uid` = '" . $_SESSION['uid'] . "';";
    mysql_query($sql);
    $result = mysql_query($sql);
    $row = mysql_fetch_assoc($result);
    $encpasswd = explode('$', $row['password']);
    $rand = $encpasswd[0];
    $hash = $encpasswd[1];
    if (md5($rand . '$' . $oldpw) != $hash) {
        $error = '旧密码错误。';
        break;
    }

    $rand = rand(10000, 100000);
    $newpw = $rand . '$' . $newpw;
    $encpasswd = $rand . '$' . md5($newpw);
    $sql = "UPDATE `vf_users` SET `password` = '" . addslashes($encpasswd) . "' WHERE `uid` = '" . $_SESSION['uid'] . "';";
    mysql_query($sql);

?><!doctype html><meta charset=utf-8><title>修改密码成功</title><h1>修改密码成功</h1><p><a href='problem.php'><strong>点击这里前往题目列表</strong></a>。<hr /><p><small>Copyright &copy; Hexchain Tong, 2011.</small><?php
    exit();
}

?><!doctype html><meta charset=utf-8><title>修改密码</title><script type=text/javascript>var RecaptchaOptions={theme:'clean',tabindex=4};function check(e){if(!e.oldpw.value.trim()||!e.newpw.value.trim()){alert('缺少参数。');return false;};if(e.newpw.value!=e.newpwconf.value){alert('两次输入密码不一致。');return false;};return true;}</script><link rel=stylesheet type=text/css href=style.css /><h1>修改密码</h1><?php
if(isset($error) && $error) {
?><p class=error>错误：<?=$error;?></p><?php
}
?><form method=post action=changepw.php onsubmit='return check(this);'><table><tr><th>用户 ID：<td><?=$_SESSION['uid'];?><tr><th>旧密码：<td><input type=password name=oldpw tabindex=1 /><tr><th>新密码：<td><input type=password name=newpw tabindex=2 /><tr><th>确认密码：<td><input type=password name=newpwconf tabindex=3 /><tr><th>验证码：<td><?=recaptcha_get_html(RECAPTCHA_PUBKEY);?><tr><td colspan=2><input type=submit></table></form><hr /><p><small>Copyright &copy; Hexchain Tong, 2011.</small>
