<?php
require_once "config.php";
require_once "recaptchalib.php";
mysql_connect(DB_HOST, DB_USER, DB_PASS) or die(mysql_error());
mysql_select_db(DB_NAME) or die(mysql_error());
mysql_query('SET NAMES utf8;') or die(mysql_error());
session_start();

$redirloc = isset($_GET['redir']) ? $_GET['redir'] : 'problem.php';
$redirtext = ($redirloc == 'problem.php') ? '问题列表' : '登录前页面';

if(isset($_SESSION['uid'])) {
    header('Location: problem.php');
    exit();
}

switch($_SERVER['REQUEST_METHOD']) {
case 'POST':
    if(isset($_POST['username'])) { $username = trim($_POST['username']); } else { $username = ''; };
    if(isset($_POST['password'])) { $password = trim($_POST['password']); } else { $password = ''; };
    if(isset($_POST['recaptcha_challenge_field'])) { $recaptcha_challenge_field = $_POST['recaptcha_challenge_field']; } else {$recaptcha_challenge_field = ''; };
    if(isset($_POST['recaptcha_response_field'])) { $recaptcha_response_field = $_POST['recaptcha_response_field']; } else {$recaptcha_response_field = ''; };

    if((!$username) || (!$password) || (!$recaptcha_challenge_field) || (!$recaptcha_response_field)) {
        $error = '缺少参数。';
        break;
    }

    $resp = recaptcha_check_answer(RECAPTCHA_PRIVKEY, $_SERVER["REMOTE_ADDR"], $recaptcha_challenge_field, $recaptcha_response_field);
    if (!$resp->is_valid) {
        $error = '验证码错误。';
        break;
    }

    $sql = "SELECT `uid`, `password` FROM `vf_users` WHERE `username` = '" . addslashes($username) . "';";
    mysql_query($sql);
    $result = mysql_query($sql);
    $row = mysql_fetch_assoc($result);
    if (!$row) {
        $error = '用户名和/或密码错误。';
        break;
    }
    $encpasswd = explode('$', $row['password']);
    $rand = $encpasswd[0];
    $hash = $encpasswd[1];
    if (md5($rand . '$' . $password) != $hash) {
        $error = '用户名和/或密码错误。';
        break;
    }

    $_SESSION['uid'] = $row['uid'];
    ?><!doctype html><meta charset=utf-8><title>登录成功</title><h1>登录成功</h1><img src=img/256px-Choco_chip_cookie.jpg alt="A chocolate-chip cookie." width=256 height=178 /><p>请带好您的曲奇，<a href='<?=$redirloc; ?>'><strong>点击这里前往<?=$redirtext; ?></strong></a>。<hr /><p><small>Copyright &copy; Hexchain Tong, 2011.</small><?php
    exit();
}

?><!doctype html><meta charset=utf-8><title>用户登录</title><script type=text/javascript>var RecaptchaOptions={theme:'white',tabindex:3};function check(e){if(!e.username.value.trim()||!e.password.value.trim()){alert('缺少参数。');return false;};return true;}</script><link rel=stylesheet type=text/css href=style.css /><h1>用户登录</h1><?php
if(isset($error) && $error) {
?><p class=error>错误：<?=$error;?></p><?php
}
?><form method=post action="signin.php?redir=<?=$redirloc; ?>" onsubmit='return check(this);'><table><tr><th>用户名：<td><input name=username tabindex=1 /><tr><th>密码：<td><input type=password name=password tabindex=2 /><tr><th>验证码：<td><?=recaptcha_get_html(RECAPTCHA_PUBKEY);?><tr><td><input type=submit><td><a href='signup.php?redir=<?=$redirloc; ?>'?>>注册</a></table></form><hr /><p><small>Copyright &copy; Hexchain Tong, 2011.</small>
