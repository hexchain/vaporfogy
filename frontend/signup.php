<?php
require_once "config.php";
require_once "recaptchalib.php";
mysql_connect(DB_HOST, DB_USER, DB_PASS) or die(mysql_error());
mysql_select_db(DB_NAME) or die(mysql_error());
mysql_query('SET NAMES utf8;') or die(mysql_error());

$redirloc = isset($_GET['redir']) ? $_GET['redir'] : 'problem.php';

switch($_SERVER['REQUEST_METHOD']) {
case 'POST':
    if(isset($_POST['username'])) { $username = trim($_POST['username']); } else { $username = ''; };
    if(isset($_POST['password'])) { $password = trim($_POST['password']); } else { $password = ''; };
    if(isset($_POST['passwordconf'])) { $passwordconf = trim($_POST['passwordconf']); } else { $passwordconf = ''; };
    if(isset($_POST['recaptcha_challenge_field'])) { $recaptcha_challenge_field = $_POST['recaptcha_challenge_field']; } else {$recaptcha_challenge_field = ''; };
    if(isset($_POST['recaptcha_response_field'])) { $recaptcha_response_field = $_POST['recaptcha_response_field']; } else {$recaptcha_response_field = ''; };

    if((!$username) || (!$password) || (!$recaptcha_challenge_field) || (!$recaptcha_response_field)) {
        $error = '缺少参数。';
        break;
    }
    if((strlen($username) >= 32) || (strlen($password) >= 64)) {
        $error = '用户名或密码过长。';
        break;
    }
    if(preg_match('/[^a-zA-Z0-9_-]/', $username)) {
        $error = '用户名不合法。';
        break;
    }
    if($password != $passwordconf) {
        $error = '两次输入密码不一致。';
        break;
    }

    $resp = recaptcha_check_answer(RECAPTCHA_PRIVKEY, $_SERVER["REMOTE_ADDR"], $recaptcha_challenge_field, $recaptcha_response_field);
    if (!$resp->is_valid) {
        $error = '验证码错误。';
        break;
    }

    $rand = rand(10000, 100000);
    $password = $rand . '$' . $password;
    $encpasswd = $rand . '$' . md5($password);
    $sql = "INSERT INTO `vf_users` ( `username`, `password` ) VALUES ( '" . addslashes($username) . "', '" . addslashes($encpasswd) . "' );";
    if (!mysql_query($sql)) {
        $error = '同名用户已存在。';
        break;
    }
?><!doctype html><meta charset=utf-8><title>注册成功</title><h1>注册成功</h1><h2><a href='signin.php?redir=<?=$redirloc; ?>'>点击这里登录</a></h2><hr /><p><small>Copyright &copy; Hexchain Tong, 2011.</small><?php
    exit();
}

?><!doctype html><meta charset=utf-8><title>用户注册</title><script type=text/javascript>var RecaptchaOptions={theme:'white' ,tabindex:4};function check(e){if(!e.username.value.trim()||!e.password.value.trim()){alert('缺少参数。');return false;};if(/[^a-zA-Z0-9_-]/.test(e.username.value)){alert('用户名不合法。');return false;};if(e.password.value!=e.passwordconf.value){alert('两次输入密码不一致。');return false;};return true;}</script><link rel=stylesheet type=text/css href=style.css /><h1>用户注册</h1><?php
if(isset($error) && $error) {
?><p class=error>错误：<?=$error;?></p><?php
}
?><form method=post action="signup.php?redir=<?=$redirloc; ?>" onsubmit='return check(this);'><table><tr><th>用户名：<td><input name=username tabindex=1 /><tr><th>密码：<td><input type=password name=password tabindex=2 /><tr><th>确认密码：<td><input type=password name=passwordconf tabindex=3 /><tr><th>验证码：<td><?=recaptcha_get_html(RECAPTCHA_PUBKEY);?><tr><td colspan=2><input type=submit></table></form><hr /><p><small>Copyright &copy; Hexchain Tong, 2011.</small>
