<?php
require_once "config.php";
mysql_connect(DB_HOST, DB_USER, DB_PASS) or die(mysql_error());
mysql_select_db(DB_NAME) or die(mysql_error());
mysql_query('SET NAMES utf8;') or die(mysql_error());
session_start();

if(isset($_GET['rcid'])) { $rcid = (int)$_GET['rcid']; } else { $rcid = 0; };
if(isset($_GET['uid'])) { $uid = (int)$_GET['uid']; } else { $uid = 0; };
if(isset($_GET['page'])) { $page = (int)$_GET['page']; } else { $page = 0; };

$stat = array(0 => '完成', 1 => '等待', 2 => '运行', 3 => '错误', 4 => '中断');

if(!$rcid) {
    $sql = 'SELECT COUNT(*) FROM `vf_queue` ';
    if($uid) {
        $sql .= "WHERE `uid` = '" . $uid . "' ";
    }
    $sql .= ';';
    $total = mysql_fetch_assoc(mysql_query($sql));
    $total = $total['COUNT(*)'];
    $pages = (int)max(1, ceil($total / 20));
    if($page < 1) { $page = 1; };
    if($page > $pages) { $page = $pages; };
    $sql = 'SELECT * FROM `vf_queue` ';
    if($uid) {
        $sql .= "WHERE `uid` = '" . $uid . "' ";
    }
    $sql .= 'ORDER BY `rcid` DESC LIMIT ' . ($page - 1) * 20 . ', 20;';
    $result = mysql_query($sql);
    ?><!doctype html><meta charset=utf-8><title>提交队列</title><link rel=stylesheet type=text/css href=style.css /><h1>提交队列</h1><?php
    if(!isset($_SESSION['uid']) || $_SESSION['uid'] == "") {
?><a href=signin.php?redir=queue.php>登录</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href=signup.php?redir=queue.php>注册</a><br/><?php
    } else {
?>用户ID: <?=$_SESSION['uid']; ?>&nbsp;&nbsp;&nbsp;&nbsp;<a href=signout.php?redir=queue.php>注销</a><br/><?php
    }
?><table class=table><thead><tr><th>ID<th>用户 ID<th>题目 ID<th>提交时间<th>状态<th>结果<tbody><?php
    while($row = mysql_fetch_assoc($result)) {
        $back = explode("\n", $row['backmsg']);
?><tr><td><?=$row['rcid'];?><td><?=$row['uid'];?><td><a href='problem.php?prid=<?=$row['prid'];?>'><?=$row['prid'];?></a><td><?=$row['time'];?><td><a href='?rcid=<?=$row['rcid'];?>'><?=$stat[(int)$row['status']];?></a><td><?=$back[0];?><?php
    }
?></table><p><?php
    if($page == 1) {
?>&lt;Prev <?php
    } else {
?><a href='?page=<?=$page - 1;?>'>&lt;Prev</a> <?php
    }
    for($i = 1; $i <= $pages; $i++) {
        if($i == $page) {
?><?=$i;?> <?php
        } else {
?><a href='?page=<?=$i;?>'><?=$i;?></a> <?php
        }
    }
    if($page == $pages) {
?>Next&gt;<?php
    } else {
?><a href='?page=<?=$page + 1;?>'>Next&gt;</a><?php
    }
?><hr /><p><small>Copyright &copy; Hexchain Tong, 2011.</small><?php
} else {
    $sql = 'SELECT * FROM `vf_queue` WHERE `rcid` = ' . $rcid . ';';
    $result = mysql_query($sql);
    $row = mysql_fetch_assoc($result);
    if($row) {
?><!doctype html><meta charset=utf-8><title>提交详情 - <?=$row['rcid'];?></title><link rel=stylesheet type=text/css href=style.css /><h1>提交详情 - <?=$row['rcid'];?></h1><p class=sub>用户 ID：<blockquote><?=nl2br(htmlspecialchars($row['uid']));?></blockquote><p class=sub>问题 ID：<blockquote><?=nl2br(htmlspecialchars($row['prid']), true);?></blockquote><p class=sub>提交时间：<blockquote><?=nl2br(htmlspecialchars($row['time']), true);?></blockquote><p class=sub>状态：<blockquote><?=nl2br(htmlspecialchars($stat[(int)$row['status']]), true);?></blockquote><p class=sub>信息：<blockquote><pre><?=htmlspecialchars($row['backmsg']);?></pre></blockquote><hr /><p><small>Copyright &copy; Hexchain Tong, 2011.</small><?php
    } else {
        header('HTTP/1.0 404 Not Found');
?><!doctype html><meta charset=utf-8><title>提交详情</title><h1>提交 ID 不存在</h1><hr /><p><small>Copyright &copy; Hexchain Tong, 2011.</small><?php
    }
}
?>
