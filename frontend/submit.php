<?php
require_once "config.php";
require_once "recaptchalib.php";
mysql_connect(DB_HOST, DB_USER, DB_PASS) or die(mysql_error());
mysql_select_db(DB_NAME) or die(mysql_error());
mysql_query('SET NAMES utf8;') or die(mysql_error());
session_start();

if(!isset($_SESSION['uid'])) {
	$redirparam = '?redir=submit.php'.(isset($_GET['prid'])?'?prid='.$_GET['prid']:'');
    header('Location: signin.php'.$redirparam);
    exit();
}

switch($_SERVER['REQUEST_METHOD']) {
case 'POST':
    if(isset($_POST['lang'])) { $lang = $_POST['lang']; } else { $lang = ''; };
    if(isset($_POST['prid'])) { $prid = (int)$_POST['prid']; } else { $prid = 0; };
    if(isset($_POST['source'])) { $source = $_POST['source']; } else {$source = ''; };
    if(isset($_POST['recaptcha_challenge_field'])) { $recaptcha_challenge_field = $_POST['recaptcha_challenge_field']; } else {$recaptcha_challenge_field = ''; };
    if(isset($_POST['recaptcha_response_field'])) { $recaptcha_response_field = $_POST['recaptcha_response_field']; } else {$recaptcha_response_field = ''; };

    if(($lang != 'c' && $lang != 'cpp' && $lang != 'pas') || (!$prid) || (!trim($source)) || (!$recaptcha_challenge_field) || (!$recaptcha_response_field)) {
        $error = '缺少参数。';
        break;
    }

    $sql = 'SELECT `prid` FROM `vf_problems` WHERE `prid` = ' . $prid . ';';
    $result = mysql_query($sql);
    $row = mysql_fetch_assoc($result);
    if(!$row) {
        $error = '题目 ID 不存在。';
        break;
    }

    $resp = recaptcha_check_answer(RECAPTCHA_PRIVKEY, $_SERVER["REMOTE_ADDR"], $recaptcha_challenge_field, $recaptcha_response_field);
    if (!$resp->is_valid) {
        $error = '验证码错误。';
        break;
    }


    $filename = WD . 'src/' . time() . '-' . $prid . '.' . $lang;
    if(file_put_contents($filename, $source) === false) {
        $error = '无法写入临时文件。';
        break;
    }

    $sql = "INSERT INTO `vf_queue` ( `prid`, `filename`, `uid` ) VALUES ( '" . $prid . "', '" . addslashes($filename) . "', '" . $_SESSION['uid'] . "' );";
    mysql_query($sql);
    $rcid = mysql_insert_id();

    header('Location: queue.php?rcid=' . $rcid);
    exit();
}

if(isset($_GET['prid'])) { $prid = (int)$_GET['prid']; } else { $prid = 0; };

?><!doctype html><meta charset=utf-8><title>提交程序</title><script type=text/javascript>var RecaptchaOptions={theme:'white', tabindex:4};function check(e){if(!e.prid.value.trim()||e.prid.value.trim()=='0'||!e.source.value.trim()){alert('缺少参数。');return false;};return true;}</script><link rel=stylesheet type=text/css href=style.css /><h1>提交程序</h1><?php
if(isset($error) && $error) {
?><p class=error>错误：<?=$error;?></p><?php
}
?><form method=post action=submit.php onsubmit='return check(this);'><table><tr><th>用户 ID：<td><?=$_SESSION['uid'];?><tr><th>语言：<td><select name=lang tabindex=1><option value=c selected>C<option value=cpp>C++<option value=pas>Pascal</select><tr><th>题目 ID：<td><input name=prid maxlength=5 size=5 value='<?=$prid;?>' tabindex=2 /><tr><th>源代码：<td><textarea name=source rows=24 cols=80 tabindex=3></textarea><tr><th>验证码：<td><?=recaptcha_get_html(RECAPTCHA_PUBKEY);?><tr><td colspan=2><input type=submit></table></form><hr /><p><small>Copyright &copy; Hexchain Tong, 2011.</small>
